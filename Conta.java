public abstract class Conta  {

    private int numero;
    private int agencia;
    private String nomeBanco;
    protected double saldo;

    public abstract double sacar( double valor);
    public abstract double depositar (double valor);




    public int getNumero() {
        return numero;
    }

    public void setNumero( int numero ) {
        this.numero = numero;
    }

    public int getAgencia() {
        return agencia;
    }

    public void setAgencia( int agencia ) {
        this.agencia = agencia;
    }

    public String getNomeBanco() {
        return nomeBanco;
    }

    public void setNomeBanco( String nomeBanco ) {
        this.nomeBanco = nomeBanco;
    }

    public abstract double getSaldo();


    public void setSaldo( double saldo ) {
        this.saldo = saldo;
    }

    public Conta( int numero, int agencia, String nomeBanco, double saldo ) {
        this.numero = numero;
        this.agencia = agencia;
        this.nomeBanco = nomeBanco;
        this.saldo = saldo;
    }

    @Override
    public String toString() {
        return "Conta{" +
                "numero=" + numero +
                ", agencia=" + agencia +
                ", nomeBanco='" + nomeBanco + '\'' +
                ", saldo=" + saldo +
                '}';
    }
}
