public class ContaSalario extends Conta{
private int limitesDeSaques;

    public ContaSalario(  int numero, int agencia, String nomeBanco, double saldo, int limitesDeSaques ) {
        super(numero, agencia, nomeBanco, saldo);
        this.limitesDeSaques = limitesDeSaques;

    }

    @Override
    public double sacar( double valor ) {
        if (limitesDeSaques > 0 ){
            setSaldo(getSaldo() - valor);
            limitesDeSaques --;
            return  valor;
        }return 0.0;
    }

    @Override
    public double depositar( double valor ) {
        setSaldo(getSaldo() + valor);
        return valor;
    }

    @Override
    public double getSaldo() {
        return this.saldo ;
    }


}
