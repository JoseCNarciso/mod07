public class ContaPoupanca extends Conta{
    private int dataAniversario;
    private double taxaJuros;


    public ContaPoupanca( int numero, int agencia, String nomeBanco, double saldo, int dataAniversario, double taxaJuros ) {
        super(numero, agencia, nomeBanco, saldo);
        this.dataAniversario = dataAniversario;
        this.taxaJuros = taxaJuros;
    }

    @Override
    public double getSaldo() {
        return this.saldo + this.taxaJuros * this.saldo;
    }

    @Override
    public String toString() {
        return "ContaPoupanca{" +
                "dataAniversario=" + dataAniversario +
                ", taxaJuros=" + taxaJuros +
                '}';
    }

    @Override
    public double sacar( double valor ) {
        setSaldo(getSaldo() - valor);
        return valor;
    }

    @Override
    public double depositar( double valor ) {
        setSaldo(getSaldo() + valor);
        return valor;
    }
}
