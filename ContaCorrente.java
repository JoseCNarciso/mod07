public class ContaCorrente extends Conta{
    private double chequeEspecial;


    private double getChequeEspecial() {
        if( getSaldo() < 0 )
        return chequeEspecial - Math.abs(getSaldo());
        else
            return chequeEspecial;
    }

    public void setChequeEspecial( double chequeEspecial ) {
        this.chequeEspecial = chequeEspecial;
    }

    public ContaCorrente ( double valorinicialChequeEspecial,int numero, int agencia, String nomeBanco, double saldo){
        super(numero, agencia, nomeBanco, saldo);
        this.chequeEspecial = valorinicialChequeEspecial;
    }

    @Override
    public double sacar( double valor ) {
        if( valor <= (getSaldo()+ chequeEspecial)){
            setSaldo(getSaldo()- valor);
            if (getSaldo()< 0 ){
                return this.getChequeEspecial();
            }return valor;

        }else {
            System.out.println(saldo);
        }return 0.0;
    }

    @Override
    public double depositar( double valor ) {
        setSaldo(getSaldo() + valor);
        return valor;
    }


    public double getSaldo(){
        return  this.chequeEspecial + this.saldo;
    }

}
